package io.phaelmoita.test.spark.twitter

import io.phaelmoita.spark.twitter.TwitterHashTagsCombination
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.{BeforeAndAfter, BeforeAndAfterAll, FlatSpec, Matchers}

@RunWith(classOf[JUnitRunner])
class TwitterHashTagsCombinationTest extends FlatSpec with BeforeAndAfter with BeforeAndAfterAll with Matchers {

  override def beforeAll() = {}

  "resource file combinations size" should "be 11" in {
    val params = new TwitterHashTagsCombination.Params()
    params.debug = true
    params.inputPath = "./src/test/resources/test_log_tweets.json"

    val twitterHashTagsCombination = TwitterHashTagsCombination

    twitterHashTagsCombination.run(params)

    twitterHashTagsCombination.debugResult.size should equal(11)
  }
}
