package io.phaelmoita.spark.twitter.utils

class HashTagsCombination {}

object HashTagsCombination {

  def getHashTagCombinations (hashTags:Array[String]):List[Array[String]] = {
    val htSorted = hashTags.sorted
    var combinations:List[Array[String]] = List[Array[String]]()

    for (i <- 2 to (htSorted.length)) {
      combinations = combinations ++ htSorted.combinations(i).toList
    }

    combinations
  }

  def getStringsHashTagCombinations (hashTags:Array[String]):List[String] = {
    val listCombinations = HashTagsCombination.getHashTagCombinations(hashTags)
    listCombinations.map(_.mkString(" "))
  }
}
