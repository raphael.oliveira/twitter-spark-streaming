# twitter-spark-streaming

To execute this project it's needed start the [broadcast sever](https://gitlab.com/raphael.oliveira/twitter-broadcast)

**execute tests**
```
mvn test
```

**generate jar**
```
mvn clean
mvn package
```

**build image**
```
docker build -t twitter-spark-master -f Dockerfile .
```

**execute streaming with spark submit (depends on twitter-broadcast service)**
```
spark-submit \
--name twitter-spark-master \
--master local \
--class io.phaelmoita.spark.twitter.TwitterStreaming \
./target/twitter-spark-streaming-1.0.jar \
--host-stream localhost --port-stream 9009 \
--debug true
```

**execute streaming with docker (depends on twitter-broadcast service)**
```
docker run -it --rm --name twitter-spark-master -h twitter-spark-master \
-v $(pwd)/temp/:/app/temp/ \
-e ENABLE_INIT_DAEMON=false \
-e SPARK_APPLICATION_JAR_LOCATION=/app/twitter-spark-streaming-1.0.jar \
-e SPARK_APPLICATION_MAIN_CLASS=io.phaelmoita.spark.twitter.TwitterStreaming \
-e SPARK_APPLICATION_ARGS="--host-stream twitter-broadcast --port-stream 9009 --debug true" \
twitter-spark-master
```

**execute streaming with docker compose**
```
ACCESS_TOKEN='${YOUR_ACCESS_TOKEN}' \
ACCESS_SECRET='${YOUR_ACCESS_SECRET}' \
CONSUMER_KEY='${YOUR_CONSUMER_KEY}' \
CONSUMER_SECRET='${YOUR_CONSUMER_SECRET}' \
TCP_IP='0.0.0.0' \
TCP_PORT=9009 \
QUERY_WORD=':)' \
docker-compose up
```

**execute tweet combination**
```
spark-submit \
--name twitter-spark-combination \
--master local \
--class io.phaelmoita.spark.twitter.TwitterHashTagsCombination \
./target/twitter-spark-streaming-1.0.jar \
--input-path "file:///$(pwd)/temp/data/output/*.json" \
--debug true
```

**execute tweet combination with docker**
```
docker exec -it twitter-spark-master /bin/bash -c \
"/spark/bin/spark-submit \
--name twitter-spark-combination \
--master local \
--class io.phaelmoita.spark.twitter.TwitterHashTagsCombination \
/app/twitter-spark-streaming-1.0.jar \
--input-path file:///app/temp/data/output/*.json \
--debug true > /app/log_hash.txt && cat log_hash.txt"
```